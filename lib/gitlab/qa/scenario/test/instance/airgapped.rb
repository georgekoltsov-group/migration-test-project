# frozen_string_literal: true

module Gitlab
  module QA
    module Scenario
      module Test
        module Instance
          class Airgapped < Scenario::Template
            require 'resolv'
            attr_accessor :commands

            def initialize
              gitlab_ip = Resolv.getaddress('registry.gitlab.com')
              @commands = <<~AIRGAP_AND_VERIFY_COMMAND.split(/\n+/)
                # Should not fail before airgapping due to eg. DNS failure
                # Ping and wget check
                apt-get update && apt-get install -y iptables nmap
                nmap -sT #{gitlab_ip} -p 80 && (echo \"Regular connectivity nmap check passed.\" && exit 0) || (echo \"Regular connectivity nmap check failed.\" && exit 1)
                echo "Checking regular connectivity..." \
                  && wget --retry-connrefused --waitretry=1 --read-timeout=15 --timeout=10 -t 2 http://registry.gitlab.com > /dev/null 2>&1 \
                  && (echo "Regular connectivity wget check passed." && exit 0) || (echo "Regular connectivity wget check failed." && exit 1)

                iptables -P INPUT DROP && iptables -P OUTPUT DROP
                iptables -A INPUT -i lo -j ACCEPT && iptables -A OUTPUT -o lo -j ACCEPT # LOOPBACK
                iptables -I INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
                iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

                # Jenkins on port 8080 and 50000
                iptables -A OUTPUT -p tcp -m tcp --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT \
                  && iptables -A OUTPUT -p tcp -m tcp --dport 50000 -m state --state NEW,ESTABLISHED -j ACCEPT
                iptables -A OUTPUT -p tcp -m tcp --sport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
                iptables -A INPUT -p tcp -m tcp --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
                iptables -A OUTPUT -p tcp -m tcp --sport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
                iptables -A INPUT -p tcp -m tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT

                # Should now fail to ping and wget, port 80 should be open
                nmap -sT #{gitlab_ip} -p 80 && (echo \"Airgapped network faulty. Connectivity nmap check failed.\" && exit 1) || (echo \"Connectivity nmap check passed.\" && exit 0)
                nmap -sT  127.0.0.1 -p 22 && (echo "Airgapped connectivity port 22 check passed." && exit 0) || (echo "Airgapped connectivity port 22 check failed." && exit 1)
                nmap -sT 10 127.0.0.1 -p 80 && (echo "Airgapped connectivity port 80 check passed." && exit 0) || (echo "Airgapped connectivity port 80 check failed." && exit 1)
                echo "Checking airgapped connectivity..." \
                  && wget --retry-connrefused --waitretry=1 --read-timeout=15 --timeout=10 -t 2 http://registry.gitlab.com > /dev/null 2>&1 \
                  && (echo "Airgapped network faulty. Connectivity wget check failed." && exit 1) || (echo "Airgapped network confirmed. Connectivity wget check passed." && exit 0)
              AIRGAP_AND_VERIFY_COMMAND
            end

            def perform(release, *rspec_args)
              Component::Gitlab.perform do |gitlab|
                gitlab.release = release
                gitlab.network = 'test'
                gitlab.runner_network = 'airgapped'
                gitlab.exec_commands = @commands
                rspec_args << "--" unless rspec_args.include?('--')
                rspec_args << %w[--tag ~orchestrated]
                gitlab.instance do
                  Component::Specs.perform do |specs|
                    specs.suite = 'Test::Instance::Airgapped'
                    specs.release = gitlab.release
                    specs.network = gitlab.network
                    specs.runner_network = gitlab.runner_network
                    specs.args = [gitlab.address, *rspec_args]
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
