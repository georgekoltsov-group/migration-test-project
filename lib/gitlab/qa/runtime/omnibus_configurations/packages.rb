# frozen_string_literal: true

module Gitlab
  module QA
    module Runtime
      module OmnibusConfigurations
        class Packages < Default
          def configuration
            <<~OMNIBUS
              gitlab_rails['packages_enabled'] = true
            OMNIBUS
          end
        end
      end
    end
  end
end
