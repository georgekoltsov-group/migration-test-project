# frozen_string_literal: true

module Gitlab
  module QA
    module Component
      class Production < Staging
        ADDRESS = 'https://gitlab.com'
      end
    end
  end
end
