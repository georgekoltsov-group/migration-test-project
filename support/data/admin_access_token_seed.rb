# frozen_string_literal: true

class AdminAccessTokenSeed
  TOKEN_VALUE = 'ypCa3Dzb23o5nvsixwPA'
  SCOPES = Gitlab::Auth.all_available_scopes

  def self.seed!
    admin_user = User.find_by(username: 'root')

    token_params = {
      scopes: SCOPES.map(&:to_s),
      name: 'admin-api-token'
    }

    admin_user.personal_access_tokens.build(token_params).tap do |pat|
      pat.set_token(TOKEN_VALUE)
      pat.save!
    end

    puts 'Personal access token seeded for root user.'
  end
end

AdminAccessTokenSeed.seed!
